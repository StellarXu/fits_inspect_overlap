# -*- coding: utf-8 -*-
"""
Created on  May,22, 2020

@author: Xu Chen, Jilin University

This code is used to: Compare different Images, Overlap contours
    Output a pdf.
"""
import matplotlib as mpl
mpl.use('Agg')

from astropy.io import fits
from astropy import log
from matplotlib import pyplot as plt
import numpy as np
from astropy import units as u
from astropy import wcs,log
import os

from hiviewer import *
from matplotlib.backends.backend_pdf import PdfPages
#################################################################

'''
#if it doesn't have beam information(resolution)...
hdl=fits.open(filepath+'/M33_FAST_cube.fits')
hd=hdl[0].header
print(hd)

hd['CTYPE3']='VELOCITY'
hd['BMAJ']=3/60
hd['BMIN']=3/60
hd['BPA']=0.0

fits.writeto(filepath+'/M33_FAST_cube_new.fits',hdl[0].data,hd,overwrite=True)
hdl.close()
'''
######################### INPUT ##############################
file1 = './fast/M33_FAST_cube_new.fits'
name1 = 'fast'
data_unit1 = 'K'
file2 = '../../arbo/M33_arecibo.fits'
name2 = 'arbo'
data_unit2 = 'Jy/beam'
#    Arecibo data: http://www.naic.edu/~ages/public_cubes/M33_local.fits.gz
#    and article: http://adsabs.harvard.edu/abs/2016MNRAS.456..951K

pdfname = './fig/M33_FAST_arecibo_compare.pdf'

v1 = -300 #slab cube velocity(km/s)
v2 = -50 

################# convolve and reproject #############################
if os.path.exists(pdfname):
    print(f"{pdfname} already exsists.Delete it...")
    os.remove(pdfname)

#data cubes
cube1 = FitsPic(file1)
cube2 = FitsPic(file2)

if cube1.beam > cube2.beam:
    beam_target = cube1
    log.info(f'convolve beam_target is {name1}')
else:
    beam_target = cube2
    log.info(f'convolve beam_target is {name2}')
if cube1.cdelt[1] > cube2.cdelt[1]:
    reproj_target = cube1
    log.info(f'reproject_target is {name1}')
else:
    reproj_target = cube2
    log.info(f'reproject_target is {name2}')

print(cube1.data.shape,cube2.data.shape)

#slab moment0
reproj_target.slab_moment(v1,v2,filepath = './data/reproj_target')
rpj_m0 = FitsPic('./data/reproj_target_HI-moment0.fits')

#resolution and pixel size
log.info(f'convolving and reprojecting to pixel size {reproj_target.cdelt[1]} and ')
print(beam_target.beam)

#convolve and reproject
cube1.spcube_convolve_reproj(rpj_m0,beam_target,v1,v2,fitsname='./data/'+name1)
cube2.spcube_convolve_reproj(rpj_m0,beam_target,v1,v2,fitsname='./data/'+name2)
print('Finished convolve and reproject :D')

cube1.slab_moment(v1,v2,filepath = f'./data/{name1}',m0=True,m1=True)
bk_m0 = FitsPic(f'./data/{name1}_HI-moment0.fits')
bk_m1 = FitsPic(f'./data/{name1}_HI-moment1.fits')
####################### compare and overlap ######################
#using these  moment 0 maps to compare
bk1 = FitsPic(f'./data/{name1}_convolve_reproj_result.fits')
fo2 =FitsPic(f'./data/{name2}_convolve_reproj_result.fits')
print(bk1.data.shape,fo2.data.shape)

pdf = PdfPages(pdfname)

#maybe not very suitable contour levels
bk1levels = bk1.center_levels(data_unit = data_unit1)
fo2levels = fo2.center_levels(data_unit = data_unit2)
'''
#suitable contour levels
alevels=(158.4-np.logspace(1.83,2.09,23))[::-1]
flevels=(1120-np.logspace(1.9,2.8,23))[::-1]
vlevels=(1100-np.logspace(2.2,3.06,17))[::-1]
print(f"arbo levels:{alevels}")
print(f"fast levels:{alevels}")
print(f"vla levels:{vlevels}")
'''

bk_m0.plot_contour(bk1levels,vmin_max = (0,1000),alpha=0.9,clabel=False,
                     cbar_label='[K]', picname =f'./fig/{name1}m0',save = False)
                                                    #dont't save a single png
pdf.savefig()#saved in the pdf
plt.close()

overlap_img_contour(bk_m1,bk_m0,levels=bk1levels,c='k',vmin_max=None,alpha=0.8,bar_label='km/s',radec_range = [22.7,24.1,30,31.5],
                    clabel=False,save = False,picname =f'./fig/{name1}_m0_and_m1')
pdf.savefig()
plt.close()

overlap_two_contours(bk1,fo2,bklevels=bk1levels,folevels=fo2levels, radec_range = [22.7,24.1,30,31.5],
                     alphabk=0.5,c='orange',picname=f'./fig/{name1}_grey_{name2}_orange',save = False)                          
pdf.savefig()
plt.close()


#  search and mark the peaks and valleys
two_contours_with_peaks(bk1,fo2,min_distance=2,bklevels=bk1levels,folevels=fo2levels,
                        alphabk=0.4,picname=f'./fig/{name1}_grey_{name2}_pink',save = False)
pdf.savefig()
plt.close()

#  you can add other functions...for example
#plot spec in this area 
ra=np.linspace(23.0,23.5,5)#deg
dec=np.linspace(30.4,31.0,4)
for i in range(len(ra)):
    for j in range(len(dec)):
        cube1.plot_spec(ra[i],dec[j],picname = f'./fig/{name1}_{i}_{j}',save = False)
        #may be many spectrum pics...
        pdf.savefig()
        plt.close()
        


d = pdf.infodict() 
import datetime
d['ModDate'] = datetime.datetime.today()
d['Subject'] = 'Compare M33'
d['Keywords'] = 'FAST,Arecibo'
d['Author'] = 'a cute bear Knut'
d['Title'] = pdfname

pdf.close()
    
print(f" Pictures saved in {pdfname} :)")
