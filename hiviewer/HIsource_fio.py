#!/usr/bin/env python
# coding: utf-8

"""
HIsource_fio.py
Created on someday Jan, 2021
@author: Xu Chen, Jilin University

A simple of version of HIsource.py for hiviewer, is used to fit contour and plot major axis slice.

"""


import numpy as np
from matplotlib import pyplot as plt
import hiviewer as FIO
from astropy import log
import heapq

class HIsource(object):
    def __init__(self,subcube,subm0):
        self.cube = subcube
        self.m0 = subm0
    
    def find_contour_fit(self,con,center_thld=4,function = 'default'):
        """
        find a contour to fit with an ellipse
        """
        from numpy.linalg import LinAlgError
        
        len_list=list(map(len,con.allsegs[0]))
        if len(len_list) < 4: # try 4 times at most
            contour_num_max = len(len_list)
        else:
            contour_num_max = 4
        
        len_list_sort=heapq.nlargest(contour_num_max,len_list)
        for contour_num in range(contour_num_max):
            print(f"fitting contour {contour_num}")
            loc = np.where(np.array(len_list)==len_list_sort[contour_num])[0][0]

            con_loc=con.allsegs[0][loc]#[np.argmax(len_list)]
            conx_,cony_=con_loc[:,0],con_loc[:,1]
            ################## use ellipse to fit the contour ####################
            try:
                if function == 'default':
                    from .utils import ellipse_fit
                    center_,major_,minor_,phi_,para_=ellipse_fit(conx_,cony_)
                elif function == 'kapteyn':
                    from .utils import kapteyn_ellipse_fit
                    center_,major_,minor_,phi_,para_=kapteyn_ellipse_fit(conx_,cony_)

                if np.sqrt(sum((center_-self.m0.crpix)**2)) < center_thld:
                    center,major,minor,phi,para,conx,cony = center_,major_,minor_,phi_,para_,conx_,cony_
                    print("Found a near-center ellipse :D  hhh")# guess the target should stay in central area
                    
                    return center,major,minor,phi,para,conx,cony
                
                else:
                    print("Far from center.Next try...")

            except LinAlgError as L:
                print(f"{repr(L)}. Can't fit this ellipse. Next try...")
            except ValueError as V:
                print(f"{repr(V)}. Can't fit this ellipse. Next try...")
                
        log.warn("failed  ellipse fitting !")
        return []
            

    def fit_contour(self ,factor=None,levels=None,vmin_max = None,clabel=False,figsize=(6, 5),
                     cbar_label='Jy/beam', picname ='./',save = False,cmap='gray',alpha = 0.9,
                    plot_major_line=False,function = 'default',**kwargs):
        """ 
        plot the image, contours, ellipse, and get fit paraments and major line. 
        """
        
        fig = plt.figure(figsize=figsize)
        ax = fig.add_subplot(111, projection=self.m0.wcs_cel)
                                                                 #plt.rcParams['ytick.direction']='in'
        if vmin_max is None:
            im=ax.imshow(self.m0.get_slice(),alpha=alpha,cmap=cmap)
        else:
            im=ax.imshow(self.m0.get_slice(),vmin=vmin_max[0],vmax=vmin_max[1],alpha=alpha,cmap=cmap)        
        cbar = plt.colorbar(im,pad=.01)
        cbar.set_label(f'{cbar_label}', size=14)
        ax.set_xlabel("RA", fontsize=14)
        ax.set_ylabel("DEC", fontsize=14)
        [ax.coords[ci].display_minor_ticks(True) for ci in range(2)]
        if levels != None:
            contours=ax.contour(self.m0.get_slice(),levels = levels,colors='yellow')
        ################## definite a level and plot a contour ##############
        peak=np.nanmax(self.m0.data)
        if factor == None:
            factor = 0.5
        print(f"contour level to fit: {peak:.3f} * {factor}")
        con = ax.contour(self.m0.get_slice(),levels = [peak*factor] ,colors='orange')# contour to fit
        
        # plot other contours
        con_half=ax.contour(self.m0.get_slice(),levels = [peak*0.5] ,colors='r')
        con_quad=ax.contour(self.m0.get_slice(),levels = [peak*0.25] ,colors='b')
        if clabel:
            [ax.clabel(c,fmt='%.1f') for c in [con,con_half,con_quad]]
        ax.axis('equal')
        
        ax.tick_params(labelsize=11)    
        ################# find a contour to fit with an ellipse #################
        try:
            center,major,minor,phi,para,conx,cony = self.find_contour_fit(con,function=function)
            print("\n========= Fit results ellipse model ==========")
            print("Center pixel:",center)
            print("SemiMajor pixel:",major)
            print("SemiMinor pixel:",minor)
            print("Phi rad:",phi)
            print("=============== Fit Finished ===================")
        except ValueError as V:
            self.success_fit = False
            plt.show()
            log.warn(f"{repr(V)}")
            return self
        ################## get pixels position inside the ellipse ###############
#        inside_px = self.pix_inside_ellipse(para,**kwargs)
        # plot pixels inside the ellipse
#        if plot_inside_pix:
#            ax.plot(inside_px[:,0],inside_px[:,1],'m.',label='inside pixels')
        ######################## plot major line ###############################
        mx,my = self.maj_line([center,major,minor,phi])
        if plot_major_line:
            ax.plot(mx,my,'mo',label='major line')
        
        # plot FAST center
        ax.plot(center[0],center[1],'bx',label='FAST HI')
            
        ########################### plot the ellipse  ############################
        from matplotlib.patches import Ellipse
        ellipse = Ellipse(xy=center, width=2*major, height=2*minor, angle=np.rad2deg(phi),
            edgecolor='g', fc='None',label='ellipse fit',lw=2, zorder=2)
        ax.add_patch(ellipse)
        
        ax.legend(loc=2,bbox_to_anchor=(1.07,1.07),borderaxespad=2.5)
        
        if save:
            plt.savefig(picname+'_contour.png',dpi=300,bbox_inches='tight')
        plt.show()   
        
        # coefficients and inside pixels position
        self.coef = [center,major,minor,phi]
#        self.inside_px = inside_px
        self.success_fit = True
        self.majpix = [mx,my]
        return self
    
    def maj_line(self,coef):
        data = self.m0.data
        ny,nx = data.shape
        [x0,y0],_,_,phi = coef
        k = -np.tan(np.pi/2-phi)
        if nx > ny:
            x = np.arange(0,nx,1)
            y = k*(x-x0) + y0
        else:
            y = np.arange(0,ny,1)
            x = (y-y0)/k + x0

        px,py = np.around([x,y]).astype(int)
        return px,py
    
    def plot_maj_slice(self, vmin_max = None,vrange = None,clabel=False,figsize=(5,6),
                       picname ='./',save = False,cmap='gray',cbar_label = 'K'):
        
        mx,my = self.majpix
        x0,y0 = self.coef[0]
        d = (((mx-x0)**2 + (my-y0)**2)**.5)
        dist = np.hstack((d[my<y0]*(-1),d[my>=y0]))
        
        data = self.cube.data
        
        specs = np.zeros((data.shape[0],len(mx)))
        k = 0
        for i,j in zip(mx,my):
            specs[:,k] = data[:,j,i]
            k += 1
        
        from astropy import units as u
        vel = self.cube.velo.to(u.km/u.s).value
        
        if vrange is not None:
            is_use = (vel>vrange[0])&(vel<vrange[1])
            vel = vel[is_use]
            specs = specs[is_use,:]
            
        fig,ax = plt.subplots(1,1,figsize=figsize)
        if vmin_max is None:
            im=ax.imshow(specs,cmap=cmap,origin='lower',aspect='auto',
                         extent=[dist[0],dist[-1],vel[0],vel[-1]])
            plt.tight_layout()
        else:
            im=ax.imshow(specs,vmin=vmin_max[0],vmax=vmin_max[1],cmap=cmap,aspect='auto',
                         origin='lower',extent=[dist[0],dist[-1],vel[0],vel[-1]])
            plt.tight_layout()
        ax.set_ylabel(f"{self.cube.ctype[2]}[km/s]")
        ax.set_xlabel("distance [pixels]")
        cbar = plt.colorbar(im,pad=.01)
        cbar.set_label(f'{cbar_label}', size=15)
        ax.minorticks_on()
        plt.show()
        
